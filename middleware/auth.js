const jwt = require('jsonwebtoken');
const jwtConfig = require('../config/jwtConfig');

function authenticateToken(req, res, next) {
  const token = req.headers.authorization?.split(' ')[1];

  if (token === undefined) {
    return res.status(401).json({ message: 'Token tidak ditemukan' }); // Token tidak ditemukan, kirim respon Unauthorized
  }

  jwt.verify(token, jwtConfig.secretKey, (err, user) => {
    if (err) {
      return res.status(403).json({ message: err }); // Token tidak valid, kirim respon Forbidden
    }
    req.user = user; // Menyimpan informasi pengguna pada req untuk rute selanjutnya
    next(); // Lanjut ke rute berikutnya
  });
}

module.exports = {
  authenticateToken,
};
