const ShowTheater = require('../models/showTheaterModels'); // Mengimpor model ShowTheater atau entitas showTheater

exports.getAllShowTheaters = async (req, res) => {

  const limit = parseInt(req.query.limit) || 1000000; // Jumlah item per halaman
  const page = parseInt(req.query.page) || 1; // Halaman yang diminta oleh klien

  const skip = (page - 1) * limit;

  try {
    const showTheater = await ShowTheater
      .find()
      .sort({ schedule: -1 })
      .skip(skip)
      .limit(limit);
    
    const totalItems = await ShowTheater.find().countDocuments();
    const totalPages = Math.ceil(totalItems / limit);

    const hasNextPage = page < totalPages;
    const hasPrevPage = page > 1;

      res.status(200).json({
        returnContent: showTheater,
        currentPage: page,
        totalPages: totalPages,
        hasNextPage,
        hasPrevPage,
      });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Terjadi kesalahan saat mengambil data showTheater' });
  }
};

exports.addManyItems = async (req, res) => {
  try {
    const itemsToAdd = req.body.jsonData;
    const insertedItems = await ShowTheater.insertMany(itemsToAdd);
    res.status(201).json(insertedItems);
  } catch (error) {
    res.status(500).json({ message: 'Terjadi kesalahan saat menambahkan data many showTheater' });
  }
}

exports.createShowTheater = async (req, res) => {
  try {
    const newShowTheater = new ShowTheater({
      title: req.body.title,
      order: req.body.order,
      url: req.body.url,
      addInfo: req.body.addInfo,
      schedule: req.body.schedule,
      unitSong: req.body.unitSong,
      unitSong2: req.body.unitSong2,
    });
    await newShowTheater.save();
    res.status(201).json(newShowTheater);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Terjadi kesalahan saat membuat showTheater baru' });
  }
};

exports.getShowTheaterById = async (req, res) => {
  const showTheaterId = req.params.id;
  try {
    const showTheaterName = await ShowTheater.findById(showTheaterId);
    if (!showTheaterName) {
      res.status(404).json({ message: 'showTheater tidak ditemukan' });
    } else {
      res.status(200).json(showTheaterName);
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Terjadi kesalahan saat mengambil data showTheater' });
  }
};

exports.updateShowTheater = async (req, res) => {
  const showTheaterId = req.params.id;
  try {
    const updatedShowTheater = await ShowTheater.findByIdAndUpdate(showTheaterId, {
      title: req.body.title,
      order: req.body.order,
      url: req.body.url,
      addInfo: req.body.addInfo,
      schedule: req.body.schedule,
      unitSong: req.body.unitSong,
      unitSong2: req.body.unitSong2,
    }, { new: true });
    res.status(200).json(updatedShowTheater);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Terjadi kesalahan saat mengupdate showTheater' });
  }
};

exports.deleteShowTheater = async (req, res) => {
  const showTheaterId = req.params.id;
  try {
    await ShowTheater.findByIdAndRemove(showTheaterId);
    res.status(204).send();
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Terjadi kesalahan saat menghapus showTheater' });
  }
};
