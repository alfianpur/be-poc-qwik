const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const { google } = require('googleapis');
const fs = require('fs');
const readline = require('readline')

const credentials = require('../helpers/cis-google.json');
const TOKEN_PATH = 'token.json';

const oAuth2Client = new google.auth.OAuth2(
  credentials.client_id,
  credentials.client_secret,
  credentials.redirect_uris[0]
);

let transporter;

function generateOTP() {
  return Math.floor(100000 + Math.random() * 900000).toString();
}

async function getNewToken() {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: 'https://www.googleapis.com/auth/gmail.send',
  });

  console.log('Authorize this app by visiting this URL:', authUrl);
}

async function authorize() {
  try {
    const token = fs.readFileSync(TOKEN_PATH);
    return JSON.parse(token);
  } catch (err) {
    return await getNewToken();
  }
}

async function sendOTP(email, otp) {
  try {
    const token = await authorize();
    oAuth2Client.setCredentials(token);

    transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: 'calendar.alfian@gmail.com',
        clientId: credentials.client_id,
        clientSecret: credentials.client_secret,
        refreshToken: token?.refresh_token,
        accessToken: token?.access_token,
      },
    });

    const mailOptions = {
      from: 'calendar.alfian@gmail.com',
      to: email,
      subject: 'OTP Olla Addict',
      text: `Masukkan OTP ini untuk verifikasi ${otp}`,
    };

    const info = await transporter.sendMail(mailOptions);
    console.log('Email sent:', info.response);
  } catch (err) {
    console.error('Error:', err);
  }
}

module.exports = {
  generateOTP,
  sendOTP
};