const convertDate = (date) => {
  var tanggalSplit = date.split('-');
  var bulan = new Date(date + 'T00:00:00').toLocaleString('id-ID', { month: 'long' });
  var tanggalBaru = tanggalSplit[2] + ' ' + bulan + ' ' + tanggalSplit[0];
  return(tanggalBaru);
}

module.exports = {
  convertDate
};