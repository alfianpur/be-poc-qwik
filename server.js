/**
 * Mengimpor modul dan mengonfigurasi server Express untuk aplikasi.
 * @module app
 */

require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();
const db = require('./config/db'); // Mengimpor koneksi MongoDB

const allowedOrigins = [
  'http://localhost:3000', //development local fe client
  'https://be-poc-qwik.vercel.app/'
];

const corsOptions = {
  origin: function (origin, callback) {
    if (allowedOrigins.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Origin not allowed by CORS'));
    }
  },
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE', // Menentukan metode yang diizinkan
  allowedHeaders: 'Content-Type,Authorization', // Menentukan header yang diizinkan
};

app.use(cors(corsOptions));
app.use(express.json());

// Mengimpor rute yang sesuai.
const showTheaterRoutes = require('./routes/showTheaterRoutes');

// Menggunakan rute dalam aplikasi Express.
app.use('/api', showTheaterRoutes);

// Memulai server.
app.listen(process.env.PORT, () => {
  console.log(`Server is running on port ${process.env.PORT}`);
});
