const mongoose = require('mongoose');

const showTheaterSchema = new mongoose.Schema({
    title: { type: String, required: true },
    order: { type: Number, required: true },
    schedule: { type: Date, required: true },
    unitSong: {type: String, required: true },
    unitSong2: {type: String},
    url: { type: String },
    addInfo: { type: String },
});

const ShowTheater = mongoose.model('ShowTheater', showTheaterSchema);

module.exports = ShowTheater;