/**
 * Mengimpor modul dotenv dan Mongoose, dan mengonfigurasi koneksi MongoDB.
 * @module db
 */

require('dotenv').config();
const mongoose = require('mongoose');

/**
 * Membangun koneksi MongoDB menggunakan URI yang didefinisikan dalam variabel lingkungan (environment variable) MONGODB_URI.
 * @function
 * @returns {mongoose.Connection} Koneksi MongoDB yang telah dibangun.
 */
mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  // useUnifiedTopology: true,
});

/**
 * Koneksi MongoDB yang telah dibangun.
 * @type {mongoose.Connection}
 */
const db = mongoose.connection;

/**
 * Menangani kesalahan koneksi MongoDB dan mencetak pesan kesalahan ke konsol.
 */
db.on('error', console.error.bind(console, 'Kesalahan koneksi MongoDB:'));

/**
 * Menangani koneksi MongoDB yang berhasil dan mencetak pesan ke konsol.
 */
db.once('open', () => {
  console.log('Terhubung ke MongoDB');
});

module.exports = db;
