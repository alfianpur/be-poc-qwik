const express = require('express');
const router = express.Router();
const showTheaterControllers = require('../controllers/showTheaterControllers');

router.get('/show-theater', showTheaterControllers.getAllShowTheaters);

router.post('/add-many-show', showTheaterControllers.addManyItems);

router.post('/show-theater', showTheaterControllers.createShowTheater);

router.get('/show-theater/:id', showTheaterControllers.getShowTheaterById);

router.put('/show-theater/:id', showTheaterControllers.updateShowTheater);

router.delete('/show-theater/:id', showTheaterControllers.deleteShowTheater);

module.exports = router;
